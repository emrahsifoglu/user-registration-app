<?php

namespace App\Types\Enum;

use MyCLabs\Enum\Enum;

class Role extends Enum
{

    const ADMIN = 'ADMIN';
    const MEMBER = 'MEMBER';

    /**
     * @return Role
     */
    public static function ADMIN() {
        return new Role(self::ADMIN);
    }

    /**
     * @return Role
     */
    public static function MEMBER() {
        return new Role(self::MEMBER);
    }

}
