<?php

namespace App\Entity\Book;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;

class BookFacade
{

    protected $entityManager;
    protected $bookRepository;

    public function __construct(
        EntityManager $entityManager,
        BookRepository $bookRepository
    ) {
        $this->entityManager = $entityManager;
        $this->bookRepository = $bookRepository;
    }

    public function getBookRepository() {
        return $this->bookRepository;
    }

    /**
     * @return mixed
     * @throws NoResultException|NonUniqueResultException
     */
    public function getTotalCount() {
		return $this->bookRepository->findTotalCount();
	}

    /**
     * @return array
     */
    public function getBooks() {
        return $this->bookRepository->findAll();
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getBookFromId($id) {
        return $this->bookRepository->find($id);
    }

    /**
     * @param Book $book
     * @param bool $andFlush
     *
     * @throws OptimisticLockException
     */
    public function deleteBook(Book $book, $andFlush = true) {
        $this->entityManager->remove($book);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }
    
    /**
     * @param Book $book
     * @param bool $andFlush
     *
     * @return array|bool
     * @throws OptimisticLockException
     */
    public function save(Book $book, $andFlush = true) {
        try {
            $this->entityManager->persist($book);
            if($andFlush) {
                $this->entityManager->flush();
            }
            return true;
        } catch (UniqueConstraintViolationException $e) {
            return [
                'uniqueConstraint' => $e->getMessage()
            ];
        } catch (NotNullConstraintViolationException $e) {
            return [
                'notNull' => $e->getMessage()
            ];
        }
    }

}
