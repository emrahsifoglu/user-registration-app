<?php

namespace App\Entity\Book;

use App\Core\Repository\Repository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class BookRepository extends Repository
{

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager, 'App\Entity\Book\Book');
    }

    /**
     * @return mixed
     * @throws NoResultException|NonUniqueResultException
     */
    public function findTotalCount() {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Book::class, 'b')
            ->select('COUNT(b)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function findAll() {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Book::class, 'b')
            ->select('b')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function find($id) {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Book::class, 'b')
            ->select('b')
            ->where('b.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
