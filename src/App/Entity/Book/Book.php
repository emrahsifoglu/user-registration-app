<?php

namespace App\Entity\Book;

use App\Core\Entity\Entity;
use App\Entity\Author\Author;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="BookRepository")
 * @ORM\Table(name="book")
 * @ORM\HasLifecycleCallbacks()
 */
class Book extends Entity
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="isbn", type="string", length=16, nullable=false)
     */
    protected $isbn;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    protected $description;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|Author[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Author\Author", mappedBy="books")
     */
    protected $authors;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="published_at", type="date", nullable=false)
     */
    protected $publishedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct() {
        $this->authors = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->createdAt = new DateTime;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate() {
        $this->updatedAt = new DateTime;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getIsbn() {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn($isbn) {
        $this->isbn = $isbn;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|Author[]
     */
    public function getAuthors() {
        return $this->authors;
    }

    /**
     * @param Author $author
     */
    public function removeAuthor(Author $author) {
        if (false === $this->authors->contains($author)) {
            return;
        }
        $this->authors->removeElement($author);
        $author->removeBook($this);
    }

    /**
     * @return Book
     */
    public function removeAllAuthor() {
        foreach ($this->authors as $author) {
            $this->removeAuthor($author);
        }
        $this->authors->clear();

        return $this;
    }

    /**
     * @param Author $author
     */
    public function addAuthor(Author $author) {
        if (true === $this->authors->contains($author)) {
            return;
        }
        $this->authors->add($author);
        $author->addBook($this);
    }

    /**
     * @param Author[] $authors
     */
    public function addAuthors(array $authors) {
        foreach ($authors as $author) {
            $this->addAuthor($author);
        }
    }

    /**
     * @return DateTime
     */
    public function getPublishedAt() {
        return $this->publishedAt;
    }

    /**
     * @param DateTime $publishedAt
     */
    public function setPublishedAt($publishedAt) {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * @return array
     */
    public function getData() {
        return [
            'id' => $this->getId(),
            'isbn' => $this->getIsbn(),
            'title' => $this->getTitle(),
            'authors' => $this->getAuthors()->count(),
        ];
    }

}
