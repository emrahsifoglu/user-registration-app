<?php

namespace App\Entity\User;

use App\Services\Soap;
use Slim\Http\Request;

class UserRequestFacade
{

    protected $soap;

    public function __construct(
        Soap $soap
    ) {
        $this->soap = $soap;
    }

    public function createUser(Request $request) {
        $params = [
            "fullName" => $request->getParam('fullName'),
            "email" => $request->getParam('email'),
            "password" => $request->getParam('password'),
            "role" => $request->getParam('role'),
        ];

        return $this->soap->createUser($params);
    }

}
