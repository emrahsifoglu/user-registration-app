<?php

namespace App\Entity\User;

use App\Types\Enum\Role;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use SoapClient;

class UserFacade
{
    protected $soapClientOptions = [
        'cache_wsdl' => WSDL_CACHE_NONE
    ];
    protected $wsdl = "http://localhost:8000/wsdl/?WSDL";
    protected $entityManager;
    protected $userRepository;

    public function __construct(
        EntityManager $entityManager,
        UserRepository $userRepository
    ) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function getUserRepository() {
        return $this->userRepository;
    }

    public function getTotalCount() {
		return $this->userRepository->findTotalCount();
	}

    public function getUsers() {
        return $this->userRepository->findAll();
    }

    public function getUsersFromSoap() {
        try {
            $client = new SoapClient($this->wsdl, $this->soapClientOptions);

            $result = $client->__soapCall("getUsers", []); 
            $result = json_decode(json_encode($result), true);
            $result = json_decode($result['Success'], true);
 
            return array_map(function(array $userData) {
                return $this->createFromData($userData);
            }, $result);
        } catch (\SoapFault $e) {
            return [
                'Success' => false,
                'Errors' => [
                    'failure' => [$e->getMessage()]
                ]
            ];
        }
    }

    public function getUserFromEmail($email) {
        return $this->userRepository->findOneByEmail($email);
    }

    public function getUserFromId($id) {
        return $this->userRepository->findOneById($id);
    }

    public function create($fullName, $email, $password, $role) {
        if (!Role::isValid($role)) {
            return false;
        }

        $user = new User();
        $user->setFullName($fullName);
        $user->setEmail($email);
        $user->setPassword(password_hash($password, PASSWORD_DEFAULT));
        $user->setRole(new Role($role));
        return $user;
    }

    public function createFromData($data) {
        $user = new User();
        $user->setId($data['id']);
        $user->setFullName($data['fullName']);
        $user->setEmail($data['email']);
        $user->setRole(new Role($data['role']));
        return $user;
    }

    /**
     * @param User $user
     * @param bool $andFlush
     *
     * @return array|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(User $user, $andFlush = true) {
        try {
            $this->entityManager->persist($user);
            if($andFlush) {
                $this->entityManager->flush();
            }
            return true;
        } catch (UniqueConstraintViolationException $e) {
            return [
                'email' => 'Email is already used'
            ];
        } catch (NotNullConstraintViolationException $e) {
            return [
                'failure' => 'Value can not be null'
            ];
        }
    }

}
