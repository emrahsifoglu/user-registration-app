<?php

namespace App\Entity\User;

use App\Core\Repository\Repository;
use Doctrine\ORM\EntityManager;

class UserRepository extends Repository
{

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager, 'App\Entity\User\User');
    }

	public function findTotalCount() {
		return $this->getEntityManager()->createQueryBuilder()
            ->from(User::class, 'u')
			->select('COUNT(u)')
			->getQuery()
			->getSingleScalarResult();
	}

	public function findAll() {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->getQuery()
            ->getResult();
    }

    public function findOneByEmail($email) {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneById($id) {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(User::class, 'u')
            ->select('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
