<?php

namespace App\Entity\Author;

use App\Core\Entity\Entity;
use App\Entity\Book\Book;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AuthorRepository")
 * @ORM\Table(name="author")
 * @ORM\HasLifecycleCallbacks()
 */
class Author extends Entity
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="full_name", type="string", length=255, nullable=false)
     */
    protected $fullName;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|Book[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Book\Book", inversedBy="authors")
     * @ORM\JoinTable(name="author_book",
     *     joinColumns={@ORM\JoinColumn(name="author_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="book_id", referencedColumnName="id")}
     * )
     */
    private $books;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct() {
        $this->books = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() {
        $this->createdAt = new DateTime;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate() {
        $this->updatedAt = new DateTime;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName() {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection|Book[]
     */
    public function getBooks() {
        return $this->books;
    }

    /**
     * @param Book $book
     */
    public function removeBook(Book $book) {
        if (false === $this->books->contains($book)) {
            return;
        }
        $this->books->removeElement($book);
        $book->removeAuthor($this);
    }

    /**
     * @param Book $book
     */
    public function addBook(Book $book) {
        if (true === $this->books->contains($book)) {
            return;
        }
        $this->books->add($book);
        $book->addAuthor($this);
    }
    
}
