<?php

namespace App\Entity\Author;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;

class AuthorFacade
{

    protected $entityManager;
    protected $authorRepository;

    public function __construct(
        EntityManager $entityManager,
        AuthorRepository $authorRepository
    ) {
        $this->entityManager = $entityManager;
        $this->authorRepository = $authorRepository;
    }

    public function getAuthorRepository() {
        return $this->authorRepository;
    }

    /**
     * @return mixed
     * @throws NoResultException|NonUniqueResultException
     */
    public function getTotalCount() {
		return $this->authorRepository->findTotalCount();
	}

    /**
     * @return array
     */
    public function getAuthors() {
        return $this->authorRepository->findAll();
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getAuthorFromId($id) {
        return $this->authorRepository->find($id);
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getAuthorFromIds(array $ids) {
        return $this->authorRepository->findByIds($ids);
    }

    /**
     * @param Author $author
     * @param bool $andFlush
     *
     * @throws OptimisticLockException
     */
    public function deleteAuthor(Author $author, $andFlush = true) {
        $this->entityManager->remove($author);
        if($andFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param Author $author
     * @param bool $andFlush
     *
     * @return array|bool
     * @throws OptimisticLockException
     */
    public function save(Author $author, $andFlush = true) {
        try {
            $this->entityManager->persist($author);
            if($andFlush) {
                $this->entityManager->flush();
            }
        } catch (UniqueConstraintViolationException $e) {
            return [
                'uniqueConstraint' => $e->getMessage()
            ];
        } catch (NotNullConstraintViolationException $e) {
            return [
                'notNull' => $e->getMessage()
            ];
        }
    }

}
