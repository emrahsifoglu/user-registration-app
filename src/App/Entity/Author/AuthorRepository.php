<?php

namespace App\Entity\Author;

use App\Core\Repository\Repository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class AuthorRepository extends Repository
{

    public function __construct(EntityManager $entityManager) {
        parent::__construct($entityManager, 'App\Entity\Author\Author');
    }

    /**
     * @return mixed
     * @throws NoResultException|NonUniqueResultException
     */
    public function findTotalCount() {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Author::class, 'a')
            ->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function findAll() {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Author::class, 'a')
            ->select('a')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function find($id) {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Author::class, 'a')
            ->select('a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function findByIds(array $ids) {
        return $this->getEntityManager()->createQueryBuilder()
            ->from(Author::class, 'a')
            ->select('a')
            ->where('a.id in (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $searchQuery
     * @param null|int $limit
     *
     * @return array
     */
    public function findAllByPartialQuery($searchQuery, $limit = null) {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from(Author::class, 'a')
            ->where('a.fullName like :searchQuery')
            ->setParameter('searchQuery', '%'. $searchQuery . '%')
            ->getQuery();

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getResult();
    }

}
