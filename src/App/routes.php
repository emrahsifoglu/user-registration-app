<?php

namespace App;

$routes = [
    '/' => [
        [
            'name' => 'home',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'HomeController:index'
        ]
    ],
    '/author' => [
        [
            'name' => 'author',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'AuthorController:index'
        ],
    ],
    '/author/load' => [
        [
            'name' => 'author_load',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'AuthorController:loadAuthor'
        ],
    ],
    '/book' => [
        [
            'name' => 'book',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'BookController:index'
        ]
    ],
    '/register' => [
        [
            'name' => 'register',
            'methods' => ['GET', 'POST'],
            'pattern' => '',
            'callable' => 'RegisterController:register'
        ]
    ],
    '/login' => [
        [
            'name' => 'login',
            'methods' => ['GET', 'POST'],
            'pattern' => '',
            'callable' => 'LoginController:login'
        ]
    ],
    '/admin/user' => [
        [
            'name' => 'admin_user',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'Admin\UserController:index'
        ],
    ],
    '/admin/author/{id:[0-9]+}/delete' => [
        [
            'name' => 'author_delete',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'Admin\AuthorController:delete'
        ],
    ],
    '/admin/author/{id:[0-9]+}/edit' => [
        [
            'name' => 'author_edit',
            'methods' => ['GET', 'POST'],
            'pattern' => '',
            'callable' => 'Admin\AuthorController:author'
        ],
    ],
    '/admin/author/create' => [
        [
            'name' => 'author_create',
            'methods' => ['GET', 'POST'],
            'pattern' => '',
            'callable' => 'Admin\AuthorController:author'
        ],
    ],
    '/admin/book/{id:[0-9]+}/delete' => [
        [
            'name' => 'book_delete',
            'methods' => ['GET'],
            'pattern' => '',
            'callable' => 'Admin\BookController:delete'
        ],
    ],
    '/admin/book/{id:[0-9]+}/edit' => [
        [
            'name' => 'book_edit',
            'methods' => ['GET', 'POST'],
            'pattern' => '',
            'callable' => 'Admin\BookController:book'
        ],
    ],
    '/admin/book/create' => [
        [
            'name' => 'book_create',
            'methods' => ['GET', 'POST'],
            'pattern' => '',
            'callable' => 'Admin\BookController:book'
        ],
    ],
    '/logout' => [
        [
            'name' => 'logout',
            'methods' => ['GET'],
            'pattern' => '/',
            'callable' => 'AuthController:logout'
        ]
    ],
    '/wsdl' => [
        [
            'name' => 'wsdl',
            'methods' => ['GET'],
            'pattern' => '/',
            'callable' => 'SoapController:wsdl'
        ]
    ]
];
