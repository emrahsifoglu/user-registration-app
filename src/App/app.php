<?php

namespace App;

use Slim\Exception\NotFoundException;

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

$app->add(new \Slim\Middleware\Session([
    'name' => 'slim_app_session',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

$container = $app->getContainer();