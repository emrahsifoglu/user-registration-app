<?php

namespace App\Security;

use App\Entity\User\User;
use App\Entity\User\UserFacade;
use App\Types\Enum\Role;
use SlimSession\Helper as SlimSessionHelper;

class Authenticator
{

    protected $userFacade;
    protected $session;

    public function __construct(
        UserFacade $userFacade,
        SlimSessionHelper $session
    ) {
        $this->userFacade = $userFacade;
        $this->session = $session;
    }

    public function auth($email, $password) {
        /** @var User|null $user */
        $user = $this->userFacade->getUserFromEmail($email);

        if (!$user) {
            return false;
        }

        if (password_verify($password, $user->getPassword())) {
            $this->session->set('id', $user->getId());
            return true;
        }

        return false;
    }

    public function check() {
        return $this->session->get('id') !== null;
    }

    /**
     * @return User|null
     */
    public function user() {
        if (!$this->check()) {
            return null;
        }

        return $this->userFacade->getUserFromId($this->session->get('id'));
    }

    public function isMember() {
        if ($user = $this->user()) {
            return $user->getRole()->getValue() === Role::MEMBER;
        }
        return false;
    }

    public function isAdmin() {
        if ($user = $this->user()) {
            return $user->getRole()->getValue() === Role::ADMIN;
        }
        return false;
    }

    public function logout() {
        $this->session->delete('id');
    }

}
