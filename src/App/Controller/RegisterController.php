<?php

namespace App\Controller;

use App\Entity\User\UserFacade;
use App\Entity\User\UserRequestFacade;
use App\Types\Enum\Role;
use Awurth\SlimValidation\Validator;
use Slim\Http\Request;
use Slim\Http\Response;
use Respect\Validation\Validator as Respect;
use Slim\Interfaces\RouterInterface;
use Slim\Views\Twig;

class RegisterController
{

    protected $router;
    protected $twig;
    protected $validator;
    protected $userRequestFacade;

    public function __construct(
        RouterInterface $router,
        Twig $twig,
        Validator $validator,
        UserRequestFacade $userRequestFacade
    ) {
        $this->router = $router;
        $this->twig = $twig;
        $this->validator = $validator;
        $this->userRequestFacade = $userRequestFacade;
    }
    public function register(Request $request, Response $response) {
        $isValid = false;
        $csrfStatus = $request->getAttribute('csrf_status') !== false;
        $isPost = $request->isPost();
        $isSubmitted = $isPost && $csrfStatus;

        if ($csrfStatus === false) {
            $errors = [
                'failure' => ['CSRF token is not valid']
            ];
            $this->validator->setErrors($errors);
        }

        if ($isSubmitted) {
            $this->validator->validate($request, [
                'fullName' => Respect::notBlank()->alnum('_')->noWhitespace()->setName('Full name'),
                'email' => Respect::notBlank()->email()->setName('Email'),
                'password' => Respect::notBlank()->noWhitespace()->setName('Password'),
                'confirm_password' => [
                    'rules' => Respect::notBlank()->noWhitespace()->equals($request->getParam('password')),
                    'messages' => [
                        'equals' => 'Confirm password must be equals to equals',
                        'notBlank' => 'Confirm password must not be blank',
                    ]
                ],
                'role' => Respect::oneOf(
                    Respect::equals(Role::ADMIN),
                    Respect::equals(Role::MEMBER)
                )->setName('Role')
            ]);

            $isValid = $this->validator->isValid();
        }

        if ($isSubmitted && $isValid) {
            $result = $this->userRequestFacade->createUser($request);

            if ($result['Success'] === true && empty($result['Errors']) && empty($errors)) {
                return $response->withRedirect($this->router->pathFor('login'));
            }

            $this->validator->setErrors($result['Errors']);
        }

        return $this->twig->render($response, 'register.twig', [
            'roles' => Role::toArray(),
        ]);
    }

}
