<?php

namespace App\Controller;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeController
{

    protected $container;

    public function __construct(
        Container  $container
    ) {
        $this->container = $container;
    }

    public function index(Request $request, Response $response) {
        return $this->container['view']->render($response, 'home.twig');
    }

}
