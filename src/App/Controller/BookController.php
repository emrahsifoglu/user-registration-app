<?php

namespace App\Controller;

use Pimple\Container;

class BookController
{

    protected $container;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;
    }

    /**
     * @param $request
     * @param $response
     *
     * @return mixed
     */
    public function index($request, $response) {
        $books = $this->container['book_facade']->getBooks();
        return $this->container['view']->render($response, 'Book/index.twig', [
            'books' => $books
        ]);
    }

}
