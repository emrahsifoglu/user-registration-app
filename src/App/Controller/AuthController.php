<?php

namespace App\Controller;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class AuthController
{

    protected $container;

    public function __construct(
        Container  $container
    ) {
        $this->container = $container;
    }

    public function logout(Request $request, Response $response) {
        $this->container['Authenticator']->logout();
        return $response->withRedirect($this->container['router']->pathFor('home'));
    }

}
