<?php

namespace App\Controller;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;

class SoapController
{

    protected $container;
    protected $server;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;
    }

    public function wsdl(Request $request, Response $response) {
        if ($request->getQueryParam('WSDL') !== null) {
            return $this->container['wsdl']->getSoapXml();
        } else {
            $this->container['wsdl']->getOutput();
        }
    }

}
