<?php

namespace App\Controller;

use App\Entity\Author\Author;
use App\Entity\Author\AuthorFacade;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class AuthorController
{

    protected $twig;
    protected $authorFacade;

    public function __construct(
        Twig $twig,
        AuthorFacade $authorFacade
    ) {
        $this->twig = $twig;
        $this->authorFacade = $authorFacade;
    }

    /**
     * @param $request
     * @param $response
     *
     * @return mixed
     */
    public function index($request, $response) {
        $authors = $this->authorFacade->getAuthors();
        return $this->twig->render($response, 'Author/index.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadAuthor(Request $request, Response $response) {
        $name = $request->getParam('q');

        $totalCount = $this->authorFacade->getTotalCount();
        $authors = $this->authorFacade->getAuthorRepository()->findAllByPartialQuery($name);

        $items = [];

        /** @var Author $author */
        foreach ($authors as $author) {
            $items[] = [
                'id' => $author->getId(),
                'name' => $author->getFullName()
            ];
        }

        return $response->withJson([
            'items' => $items,
            'totalCount' => $totalCount
        ]);

    }

}
