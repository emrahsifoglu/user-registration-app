<?php

namespace App\Controller;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Respect\Validation\Validator as Respect;

class LoginController
{

    protected $container;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;
    }

    public function login(Request $request, Response $response) {
        $isValid = false;

        $csrfStatus = $request->getAttribute('csrf_status') !== false;
        $isPost = $request->isPost();
        $isSubmitted = $isPost && $csrfStatus;

        if ($csrfStatus === false) {
            $errors = [
                'failure' => ['CSRF token is not valid']
            ];
            $this->container['validator']->setErrors($errors);
        }

        if ($isSubmitted) {
            $this->container['validator']->validate($request, [
                'email' => Respect::notBlank()->email()->setName('Email'),
                'password' => Respect::notBlank()->noWhitespace()->setName('Password'),
            ]);

            $isValid = $this->container['validator']->isValid();
        }

        if ($isSubmitted && $isValid) {
            $auth = $this->container['Authenticator']->auth(
                $request->getParam('email'),
                $request->getParam('password')
            );

            if($auth) {
                return $response->withRedirect($this->container['router']->pathFor('home'));
            }

            $errors = [
                'failure' => ['Login is failed']
            ];
            $this->container['validator']->setErrors($errors);
        }

        return $this->container['view']->render($response, 'login.twig');
    }

}
