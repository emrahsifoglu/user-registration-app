<?php

namespace App\Controller\Admin;

use App\Entity\Author\AuthorFacade;
use App\Entity\Book\Book;
use App\Entity\Book\BookFacade;
use Awurth\SlimValidation\Validator;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Respect\Validation\Validator as Respect;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Interfaces\RouterInterface;
use Slim\Views\Twig;

class BookController
{

    protected $router;
    protected $twig;
    protected $validator;
    protected $bookFacade;
    protected $authorFacade;

    public function __construct(
        RouterInterface $router,
        Twig $twig,
        Validator $validator,
        BookFacade $bookFacade,
        AuthorFacade $authorFacade
    ) {
        $this->router = $router;
        $this->twig = $twig;
        $this->validator = $validator;
        $this->bookFacade = $bookFacade;
        $this->authorFacade = $authorFacade;
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function delete(Request $request, Response $response) {
        $book = $this->bookFacade->getBookFromId($request->getAttribute('id'));
        if ($book) {
            $this->bookFacade->deleteBook($book);
        }
        return $response->withRedirect($this->router->pathFor('book'));
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return BookController|\Psr\Http\Message\ResponseInterface
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function book(Request $request, Response $response) {
        $action = 'create';
        $book = new Book();
        $isValid = false;
        $csrfStatus = $request->getAttribute('csrf_status') !== false;
        $isPost = $request->isPost();
        $isSubmitted = $isPost && $csrfStatus;

        if ($request->getAttribute('id')) {
            $book = $this->bookFacade->getBookFromId($request->getAttribute('id'));
            $action = 'edit';
        }

        if ($csrfStatus === false) {
            $errors = [
                'failure' => ['CSRF token is not valid']
            ];
            $this->validator->setErrors($errors);
        }

        if ($isSubmitted) {
            $authors = [];
            if ($ids = $request->getParam('authors')) {
                $authors = $this->authorFacade->getAuthorFromIds($ids);
            }

            $book->setIsbn($request->getParam('isbn'));
            $book->setTitle($request->getParam('title'));
            $book->setDescription($request->getParam('description'));
            $book->setPublishedAt(date_create_from_format( 'd-m-Y', $request->getParam('publishedAt')));
            $book->removeAllAuthor()->addAuthors($authors);

            $this->validator->validate($request, [
                'isbn' => Respect::stringType()->length(9,16)->notBlank()->setName('isbn'),
                'title' => Respect::stringType()->length(2, 255)->notBlank()->setName('title'),
                'description' => Respect::notBlank()->setName('description'),
                'publishedAt' => Respect::date('d-m-Y')->setName('publishedAt'),
            ]);
            $isValid = $this->validator->isValid();
        }

        if ($isSubmitted && $isValid) {
            $this->bookFacade->save($book);
            return $response->withRedirect($this->router->pathFor('book'));
        }

        return $this-$this->twig->render($response, 'Admin\Book\book.twig', [
            'book' => $book,
            'action' => $action
        ]);
    }
}
