<?php

namespace App\Controller\Admin;

use Pimple\Container;

class UserController
{

    protected $container;

    public function __construct(
        Container $container
    ) {
        $this->container = $container;
    }

    public function index($request, $response) {
        $users = $this->container['user_facade']->getUsersFromSoap();
        return $this->container['view']->render($response, 'Admin\User\index.twig', [
            'users' => $users
        ]);
    }

}
