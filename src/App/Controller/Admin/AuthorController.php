<?php

namespace App\Controller\Admin;

use App\Entity\Author\Author;
use App\Entity\Author\AuthorFacade;
use Awurth\SlimValidation\Validator;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Respect\Validation\Validator as Respect;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Interfaces\RouterInterface;
use Slim\Views\Twig;

class AuthorController
{

    protected $router;
    protected $twig;
    protected $validator;
    protected $authorFacade;

    public function __construct(
        RouterInterface $router,
        Twig $twig,
        Validator $validator,
        AuthorFacade $authorFacade
    ) {
        $this->router = $router;
        $this->twig = $twig;
        $this->validator = $validator;
        $this->authorFacade = $authorFacade;
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function delete(Request $request, Response $response) {
        $author = $this->authorFacade->getAuthorFromId($request->getAttribute('id'));
        if ($author) {
            $this->authorFacade->deleteAuthor($author);
        }
        return $response->withRedirect($this->router->pathFor('author'));
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return AuthorController|\Psr\Http\Message\ResponseInterface
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function author(Request $request, Response $response) {
        $action = 'create';
        $author = new Author();
        $isValid = false;
        $csrfStatus = $request->getAttribute('csrf_status') !== false;
        $isPost = $request->isPost();
        $isSubmitted = $isPost && $csrfStatus;

        if ($request->getAttribute('id')) {
            $author = $this->authorFacade->getAuthorFromId($request->getAttribute('id'));
            $action = 'edit';
        }

        if ($csrfStatus === false) {
            $errors = [
                'failure' => ['CSRF token is not valid']
            ];
            $this->validator->setErrors($errors);
        }

        if ($isSubmitted) {
            $author->setFullName($request->getParam('fullName'));
            $this->validator->validate($request, [
                'fullName' => Respect::stringType()->length(2, 255)->notBlank()->setName('Full name'),
            ]);
            $isValid = $this->validator->isValid();
        }

        if ($isSubmitted && $isValid) {
            $this->authorFacade->save($author);
            return $response->withRedirect($this->router->pathFor('author'));
        }

        return $this-$this->twig->render($response, 'Admin\Author\author.twig', [
            'author' => $author,
            'action' => $action
        ]);
    }

}
