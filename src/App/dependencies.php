<?php

namespace App;

use App\Controller\AuthController;
use App\Controller\AuthorController;
use App\Controller\BookController;
use App\Controller\HomeController;
use App\Controller\LoginController;
use App\Controller\RegisterController;
use App\Controller\SoapController;
use App\Entity\Author\AuthorFacade;
use App\Entity\Author\AuthorRepository;
use App\Entity\Book\BookFacade;
use App\Entity\Book\BookRepository;
use App\Entity\User\UserFacade;
use App\Entity\User\UserRepository;
use App\Entity\User\UserRequestFacade;
use App\Security\Authenticator;
use App\Services\Soap;
use App\Services\WSDL;
use App\Twig\Extensions\CsrfExtension;
use Awurth\SlimValidation\Validator;
use Awurth\SlimValidation\ValidatorExtension;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Slim\Csrf\Guard;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use SlimSession\Helper as SessionHelper;

$container['test_container'] = function() {
    return 'It works!';
};

$container['em'] = function ($c) {
    $entityPath = [
        __DIR__ . '../../src/App/Entity'
    ];
    $isDevMode = false;

    $ini = parse_ini_file('../config/local.ini');
    $connectionOptions = [
        'driver'   => $ini['driv'],
        'host'     => $ini['host'],
        'dbname'   => $ini['name'],
        'user'     => $ini['user'],
        'password' => $ini['pass'],
    ];

    $config = Setup::createAnnotationMetadataConfiguration($entityPath, $isDevMode, null, null, false);
    return EntityManager::create($connectionOptions, $config);
};

$container['session'] = function ($c) {
    return new SessionHelper();
};

$container['csrf'] = function ($c) {
    $guard = new Guard();
    $guard->setFailureCallable(function ($request, $response, $next) {
        $request = $request->withAttribute("csrf_status", false);
        return $next($request, $response);
    });
    return $guard;
};

$container['Authenticator'] = function ($c) {
    return new Authenticator($c['user_facade'], $c['session']);
};

$container['view'] = function ($container) {
    $view = new Twig('../src/App/Resources/views', [
        'cache' => false
    ]);

    $view->addExtension(new TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    $view->addExtension(new ValidatorExtension(
        $container['validator']
    ));

    $view->addExtension(new CsrfExtension(
        $container['csrf']
    ));

    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container['Authenticator']->check(),
        'user' => $container['Authenticator']->user(),
        'is_granted_member' => $container['Authenticator']->isMember(),
        'is_granted_admin' => $container['Authenticator']->isAdmin()
    ]);

    return $view;
};

$container['validator'] = function ($c) {
    return new Validator();
};

$container['wsdl'] = function ($c) {
    $soapAddress = "http://localhost/user-registration-app/web/soap.php";

    $testSoap = [
        "funcName" => "testSoap",
        "doc" => "Test soap",
        "inputParams" => [],
        "outputParams" => [
            ["name" => "Success", "type" => "string"],
        ],
        "soapAddress" => $soapAddress
    ];

    $getUsers = [
        "funcName" => "getUsers",
        "doc" => "Load all users",
        "inputParams" => [],
        "outputParams" => [
            ["name" => "Success", "type" => "string"],
        ],
        "soapAddress" => $soapAddress
    ];

    $createUser = [
        "funcName" => "createUser",
        "doc" => "Create a new user",
        "inputParams" => [
            ["name" => "fullName", "type" => "string"],
            ["name" => "email", "type" => "string"],
            ["name" => "password", "type" => "string"],
            ["name" => "role", "type" => "string"]
        ],
        "outputParams" => [
            ["name" => "Success", "type" => "boolean"],
            ["name" => "Errors", "type" => "string"]
        ],
        "soapAddress" => $soapAddress
    ];

    /** @var WSDL $wsdl */
    $wsdl = new WSDL();
    $wsdl->setServiceName('User App Web Service Access Point');
    $wsdl->addFunction($testSoap);
    $wsdl->addFunction($getUsers);
    $wsdl->addFunction($createUser);
    return $wsdl;
};

$container['soap'] = function ($c) {
    return new Soap();
};

$container['user_repository'] = function ($c) {
    return new UserRepository($c['em']);
};

$container['user_facade'] = function ($c) {
    return new UserFacade($c['em'], $c['user_repository']);
};

$container['user_request_facade'] = function ($c) {
    return new UserRequestFacade($c['soap']);
};

$container['book_repository'] = function ($c) {
    return new BookRepository($c['em']);
};

$container['book_facade'] = function ($c) {
    return new BookFacade($c['em'], $c['book_repository']);
};

$container['author_repository'] = function ($c) {
    return new AuthorRepository($c['em']);
};

$container['author_facade'] = function ($c) {
    return new AuthorFacade($c['em'], $c['author_repository']);
};

$container['HomeController'] = function ($c) {
    return new HomeController($c);
};

$container['AuthorController'] = function ($c) {
    return new AuthorController($c['view'], $c['author_facade']);
};

$container['BookController'] = function ($c) {
    return new BookController($c);
};

$container['RegisterController'] = function ($c) {
    return new RegisterController($c['router'], $c['view'], $c['validator'], $c['user_request_facade']);
};

$container['LoginController'] = function ($c) {
    return new LoginController($c);
};

$container['Admin\AuthorController'] = function ($c) {
    return new \App\Controller\Admin\AuthorController($c['router'], $c['view'], $c['validator'], $c['author_facade']);
};

$container['Admin\BookController'] = function ($c) {
    return new \App\Controller\Admin\BookController($c['router'], $c['view'], $c['validator'], $c['book_facade'], $c['author_facade']);
};


$container['Admin\UserController'] = function ($c) {
    return new \App\Controller\Admin\UserController($c);
};

$container['AuthController'] = function ($c) {
    return new AuthController($c);
};

$container['SoapController'] = function ($c) {
    return new SoapController($c);
};
