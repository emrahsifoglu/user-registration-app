<?php

namespace App\Services;

use SoapClient;
use SoapFault;

class Soap
{
    protected $soapClientOptions = [
        'cache_wsdl' => WSDL_CACHE_NONE
    ];
    protected $wsdl = "http://localhost:8000/wsdl/?WSDL";

    public function createUser(array $params) {
        try {
            $client = new SoapClient($this->wsdl, $this->soapClientOptions);

            $result = $client->__soapCall("createUser", [$params]);
            $result = json_decode(json_encode($result), true);

            if ($result['Errors']) {
                $errorsTmp = json_decode($result['Errors'], true);
                $errors = [];
                foreach ($errorsTmp as $filed => $error) {
                    $errors[$filed][] = $error;
                }

                $result['Errors'] = $errors;
            }

            return $result;
        } catch (SoapFault $e) {
            return [
                'Success' => false,
                'Errors' => [
                    'failure' =>  [$e->getMessage()]
                ]
            ];
        }
    }

}
