# User Registration App

An application for registering users via SOAP web service into MySQL database.

## Description

Web service needs WSDL/WADL description for users which want to use their own procedure to register without HTML form. 

Service will also provide data of all registered users and their details.

* Name
* Password
* Email
* Permissions (Administrator/User)

## Table of Contents

* [Getting Started](#getting-started)
  * [Prerequisite](#prerequisite)
  * [Installation](#installation)
* [Running](#running)
* [Testing](#testing)
* [Built with](#built-with)
  * [Development Environment](#development-environment)
  * [Third-Party Dependencies](#third-party-dependencies)
* [Authors](#authors)
* [License](#license)
* [Resources](#resources)

## Getting Started

These instructions will guide you to up and running the project on your local machine.

### Prerequisite

* Composer 
* Apache( or Nginx)
* MySQL

### Installation

Virtual host must be configured for the domain. 

First move [.conf](user-registration-app.conf) file to /etc/apache2/sites-enabled/. 

Then run ```cd /etc/apache2/sites-enabled/ && ln -s /etc/apache2/sites-available/user-registration-app.conf```.

Now you install can dependencies using ```composer install```.

You must also update parameters in config/local.ini.temp but save this file as local.ini.

``` ini
[database]
driv = 'pdo_mysql'
host = 'localhost'
user = 'root'
pass = ''
name = 'user-registration-app'

```

You either import [db.sql](db.sql) to MySQL database manually or simply run ```vendor/bin/doctrine orm:schema:update --force```.

## Running

You can start the server with `composer run-script serve`. Below you may find route list.

| Method    | Path                                                             | Info          | Auth  |
| --------- | ---------------------------------------------------------------- | ------------- | ----- |
| GET       | [/](http://dev.user-registration-app.com)                        | Home page     |       |
| GET, POST | [/login](http://dev.user-registration-app.com/login)             | Login page    |       |
| GET, POST | [/register](http://dev.user-registration-app.com/register)       | Register page |       |
| GET       | [/author](http://dev.user-registration-app.com/author)           | Authors list  |       |
| GET       | [/book](http://dev.user-registration-app.com/book)               | Books list    |       |
| GET       | [/admin/user](http://dev.user-registration-app.com/admin/user)   | Users list    | ADMIN |

Admin will also be allowed to perform CRUD operation on Author and Book. 

## Testing

You can import [project.xml](user-registration-app-soapui-project.xml) into SoapUI to make SOAP requests.

![Scheme](images/soapui-project.png)

## Built with

#### Development Environment
* [Ubuntu](https://www.ubuntu.com/download/server) - Server
* [MySQL](https://www.mysql.com/) - Database
* [PHP](http://php.net/) - Language
* [Linux Mint](https://www.linuxmint.com/) - OS
* [PhpStorm](https://www.jetbrains.com/phpstorm/) - IDE
* [Chromium](https://www.chromium.org/Home) - Browser
* [Firefox quantum](https://www.mozilla.org/en-US/firefox/) - Browser
* [SoapUI](https://www.soapui.org/) - Used to test for SOAP API

#### Third-Party Dependencies
* [Slim Framework](https://www.slimframework.com/) - Web framework
* [Doctrine](http://www.doctrine-project.org/projects/orm.html) - Database abstraction layer
* [Slim Twig-view](https://www.slimframework.com/docs/features/templates.html) - Template engine
* [Respect validation](http://respect.github.io/Validation/) - Validation engine
* [Slim session](https://github.com/bryanjhv/slim-session) - Session middleware 
* [Slim validation](https://github.com/awurth/SlimValidation) - Validator
* [PHP enum](https://github.com/myclabs/php-enum) - PHP Enum implementation
* [CSRF Protection](https://github.com/slimphp/Slim-Csrf) - CSRF Protection

## Authors

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](https://github.com/emrahsifoglu)

## License

This project is licensed under the [MIT License](http://opensource.org/licenses/MIT).

## Resources

- https://wiki.gandi.net/en/hosting/using-linux/tutorials/ubuntu/virtualhosts
- http://manpages.ubuntu.com/manpages/zesty/man8/a2ensite.8.html
- http://torntech.com/database/doctrine-2-many-to-many-associations/
- https://www.bootply.com/sY7gQy6XF7
- https://www.goodreads.com/list/show/6220.The_best_of_Franz_Kafka