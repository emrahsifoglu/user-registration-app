<?php

namespace App\Core\Repository;

use App\Core\Entity\Entity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;

abstract class Repository {

    /**
     * @var EntityManager
     */
    private $entityManager;
    private $entityName;

    /**
     * Repository constructor.
     *
     * @param EntityManager $entityManager
     * @param $entityName
     */
    public function __construct(
        EntityManager $entityManager,
        $entityName
    ) {
        $this->setEntityManager($entityManager);
        $this->setEntityName($entityName);
    }

    /**
     * @param $id
     *
     * @return null|object
     */
    public function fetch($id) {
        $repository = $this->getEntityManager()->getRepository($this->getEntityName());
        $entity = $repository->find($id);

        return $entity;
    }

    /**
     * @return array
     */
    public function fetchList() {
        $repository = $this->getEntityManager()->getRepository($this->getEntityName());

        return $repository->findAll();
    }

    /**
     * @param Entity $entity
     *
     * @return int
     * @throws OptimisticLockException
     */
    public function save(Entity $entity) {
        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);

        return $entity->getId();
    }

    /**
     * @param Entity $entity
     * @throws OptimisticLockException
     */
    public function remove(Entity $entity) {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    /**
     * @return mixed
     */
    public function getEntityName() {
        return $this->entityName;
    }

    /**
     * @param $entityName
     */
    public function setEntityName($entityName) {
        $this->entityName = $entityName;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager() {
        return $this->entityManager;
    }

    /**
     * @param $entityManager EntityManager
     */
    public function setEntityManager(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

}
