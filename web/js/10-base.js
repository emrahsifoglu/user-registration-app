$(document).ready(function () {

    var url = window.location.pathname;
    var activePage = stripTrailingSlash(url);
    var section = $('#section').val();

    $('.nav li a').each(function(){
        var currentPage = stripTrailingSlash($(this).attr('href'));
        if (activePage.indexOf(currentPage) == 0 || $(this).data()['section'] == section) {
            $(this).parent().addClass('active');
            return false;
        }
    });

    $(".date-picker").datetimepicker({
        viewMode: 'months',
        format: 'DD-MM-YYYY'
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

});
