$(document).ready(function () {

    function formatRepo (repo) {
        if (repo.loading) return repo.name;

        console.log(repo.name);

        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__title'>" + repo.name + "</div>";
        markup += "</div>";

        return markup;
    }

    function formatRepoSelection (repo) {
        $('#book-authors option').each(function() {
            if ($(this).val() == repo.id) {
                $(this).text(repo.name);
            }
        });
        return repo.name || repo.text;
    }

    $("#book-authors").select2({
        placeholder: "Select author",
        allowClear: true,
        ajax: {
            url: "http://dev.user-registration-app.com/author/load",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 1,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
});
