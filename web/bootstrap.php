<?php

error_reporting(E_ALL & ~E_NOTICE);

ini_set('display_errors', 1);
ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache

require_once '../vendor/autoload.php';
require_once '../src/App/app.php';
require_once '../src/App/dependencies.php';
require_once '../src/App/routes.php';

foreach ($routes as $route => $subRoutes) {;
    foreach ($subRoutes as $pattern) {
        $route = $route . $pattern['pattern'];
        replacesDoubleSlashes($route);
        $app->map($pattern['methods'], $route, $pattern['callable'])->setName($pattern['name']);
    }
}

$app->add($container['csrf']);
