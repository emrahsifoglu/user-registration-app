<?php

require_once 'bootstrap.php';

use App\Entity\User\User;

$wsdl = "http://localhost:8000/wsdl/?WSDL";

$server = new SoapServer($wsdl);
$server->addFunction("testSoap");
$server->addFunction("getUsers");
$server->addFunction("createUser");
$server->handle();

function testSoap() {
    return [
        'Success' => 'It works!',
    ];
}

function getUsers() {
    global $container;

    $users = array_map(function(User $user){
        return $user->getData();
    }, $container['user_facade']->getUsers());

    return [
        'Success' => json_encode($users),
    ];
}

function createUser($data) {
    global $container;

    $data = get_object_vars($data);

    $fullName = $data['fullName'];
    $email = $data['email'];
    $password = $data['password'];
    $role = $data['role'];

    $error = null;
    $success = true;
    $result = null;

    $user = $container['user_facade']->create($fullName, $email, $password, $role);
    $result = ($user != false)
        ? $container['user_facade']->save($user)
        : ['failure' => 'User is invalid'];

    if ($result !== true) {
        $success = false;
        $error = json_encode($result);
    }

    return [
        'Success' => $success,
        'Errors' => $error,
    ];
}
