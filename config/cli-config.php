<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$entityPath = [
    __DIR__ . '/../src/App/Entity'
];
$isDevMode = false;

$ini = parse_ini_file('local.ini');
$connectionOptions = [
    'driver'   => $ini['driv'],
    'host'     => $ini['host'],
    'dbname'   => $ini['name'],
    'user'     => $ini['user'],
    'password' => $ini['pass'],
];

$config = Setup::createAnnotationMetadataConfiguration(
    $entityPath,
    $isDevMode,
    null,
    null,
    false
);

$entityManager = EntityManager::create($connectionOptions, $config);

return ConsoleRunner::createHelperSet($entityManager);
